#include "book.hpp"

#include <random>
#include <string>
#include <iostream>
#include <functional>
#include <chrono>

namespace Pogusch {

Book::Book() {}

Book::Book(std::string file_name) {
    init(file_name);
}

void Book::init(std::string file_name) { 
    auto start = std::chrono::system_clock::now();

    input.open(file_name);
    if(!input.good()) {
        std::cerr << "error opening book file: " << file_name << "!\n";
        root = NULL;
        book = NULL;
        return;
    }

    inited = true;
    rng = std::mt19937(time(0));
    std::string move="";
    root = new Node;
    Node* now = root;

    auto add_move = [&]() {
        bool done = 0;
        if(move == "0-1" || move == "1-0" || move == "\n" || move == "")
            return;
        for(auto [str, ch] : now->children) {
            if(str == move) {
                done = 1;
                now = ch;
                now->pop++;
                break;
            }
        }
        if(!done) {
            now->children[move] = new Node;
            now = now->children[move];
            now->pop = 1;
        }
    };

    while(1) {
        char c = input.get();
        if(c == '\n' || c == EOF) {
            // end of the game
            add_move();
            move = "";
            now = root;
            if(c == EOF)
                break;
        }
        else if(c == ' ') {
            // add move
            add_move();
            move = "";
        }
        else {
            move.push_back(c);
        }
    }

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now()-start);
    std::cerr << "book finished, took: " << duration.count() << " ms\n";
}

Book::~Book() {
    std::function<void(Node* v)> dfs = [&dfs](Node* v) {
        if(v == NULL)
            return;
        for(auto [str, ch] : v->children)
            dfs(ch);
        delete v;
    };

    dfs(root);
}

void Book::off_book() {
    book = NULL;
}

bool Book::in_book() {
    return book != NULL;
}

// true if still in book
bool Book::advance(std::string move) {
    if(book == NULL)
        return 0;    
    bool done = 0;
    for(auto [str, ch] : book->children) {
        if(str == move) {
            done = 1;
            book = ch;
            break;
        }
    }

    if(!done) {
        book = NULL;
        return 0;
    }

    return 1;
}

// picking random move from the tree
std::string Book::get_move() {
    if(book == NULL)    
        return "";
    int sum = 0;
    for(auto [str, ch] : book->children)
        sum += ch->pop;

    int token = std::uniform_int_distribution<int>(1, sum)(rng);
    std::string res = "";
    for(auto [str, ch] : book->children) {
        if(token <= ch->pop) {
            res = str;
            book = ch;
            break;
        }
        token -= ch->pop;
    }
    return res;
}

void Book::reset() {
    std::cerr << "reset!\n";
    book = root;
}

void Book::__print_root() {
    std::cerr << "-------\n";
    for(auto [str, ch] : root->children) {
        std::cerr << str << ' ' << ch->pop << '\n';
    }
    std::cerr << "-------\n";
}

void Book::__print_book() {
    std::cerr << "-------\n";
    if(book == NULL)
        std::cerr << "NULL\n";
    else
        for(auto [str, ch] : book->children) {
            std::cerr << str << ' ' << ch->pop << '\n';
        }
    std::cerr << "-------\n";
}

}