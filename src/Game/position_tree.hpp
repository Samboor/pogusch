#pragma once

#include <string>
#include <ctime>
#include <utility>
#include <iomanip>
#include <functional>
#include <algorithm>
#include <unordered_map>

#include "types.hpp"
#include "position.hpp"
#include "evaluation.hpp"

namespace Pogusch {

    class PositionTree {
    private:
        int current_depth_of_search = 0;
        int times_evaluated = 0;
        std::unordered_map<u64, double> dp;
        Position *root, *current_position;

        struct compare_pointers_to_position {
            bool operator() (const std::pair<std::string, Position*>& lhs, const std::pair<std::string, Position*>& rhs) const;
        };

        int extend_tree();

    public:
        PositionTree() = delete;
        PositionTree(const Position& position);
        ~PositionTree();

        void cleanup(std::unordered_map<u64, double>& x);
        void search(double time);

        // plays given/best (empty string) move, returns UCI move
        std::string make_move(std::string move = "");

        const Position* get_current_position() const;
        int get_depth_of_search() const;
        int get_times_evaluated() const;
    };

}