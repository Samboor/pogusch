#pragma once

#include <iostream>
#include <cctype>

#include "book.hpp"
#include "position.hpp"
#include "position_tree.hpp"

namespace UCI {

void _debug_string(std::string s, int i);
int parse_fen(std::string& cmd, int iter, Pogusch::Position& position);
int parse_moves(std::string& cmd, int iter, Pogusch::Position& position, Pogusch::Book& book);
void parse_position(std::string& cmd, Pogusch::Position& position, Pogusch::Book& book);
int get_number(int& iter, std::string& cmd);
void parse_go(std::string& cmd, Pogusch::PositionTree* position, Pogusch::Book& book);
void loop();

};
