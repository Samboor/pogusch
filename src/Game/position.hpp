#pragma once

#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>
#include <array>
#include <string>
#include <map>

#include "types.hpp"

namespace Pogusch {

    class Position {
    private:
        int move_counter = 0;
        std::array<std::array<Piece, SIZE_OF_BOARD>, SIZE_OF_BOARD> board;
        bool white_short_castle = false;
        bool white_long_castle  = false;
        bool black_short_castle = false;
        bool black_long_castle  = false;
        u64 hash = 0;
        int en_passant = -1; // au passant possible in specified column, -1 otherwise

        Color current_color = Color::White, opposite_color = Color::Black;

        mutable bool already_extended = false;
        mutable std::vector<std::pair<std::string, Position*>> next_positions;

        const Position* parent = NULL;
        mutable double evaluation = 0;

        std::array<std::array<bool, SIZE_OF_BOARD>, SIZE_OF_BOARD> generate_captures(Color color) const;

        bool empty(int row, int col) const;
        bool valid_square(int row, int col) const;
        void castle(std::pair<int, int> king, std::pair<int, int> rook, bool castling_right, std::array<std::array<bool, SIZE_OF_BOARD>, SIZE_OF_BOARD>& captures) const;
        void make_move(std::pair<int, int> begin, std::pair<int, int> end, PieceType new_piece = PieceType::None, bool au_passant_flag = false) const;

    public:
        Position(int move_count = 0);
        Position(const Position& position); // does not copy position's subtree (intentionally)
        ~Position();

        const Piece& get_piece(std::pair<int, int> square) const;
        std::vector<std::pair<std::string, Position*>>& get_next_positions() const;
        const Color& get_current_color() const;
        void place_piece(std::pair<int, int> square, PieceType type_of_piece, Color color);
        void set_castling_rights(bool white_short_castle, bool white_long_castle, bool black_short_castle, bool black_long_castle);
        void set_number_of_halfmoves(int number_of_halfmoves);
        void set_color(Color color);
        void set_en_passant(int col);
        int get_move_counter() const;

        void generate_positions(bool only_captures=0) const;
        bool valid() const;
        void generate_hash();
        u64 get_hash() const;
        void reset();

        double get_evaluation() const;
        void set_evaluation(double eval) const;
        const Position* get_parent() const;

        bool check() const;
        bool stalemate() const;
        bool checkmate() const;

        Position& operator=(const Position& position);

        friend std::ostream& operator<<(std::ostream& out, const Position& position);
    };

}