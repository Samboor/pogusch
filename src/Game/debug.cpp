#include "debug.hpp"
#include "types.hpp"
#include "uci.hpp"
#include "position.hpp"
#include "evaluation.hpp" // debug, can be removed later

#include <iostream>
#include <string>

void debug::perft() {
	std::string fen;
	int depth, _print;
	
	getline(std::cin, fen); // strange bug 
	getline(std::cin, fen);
	std::cin >> depth >> _print;

	Pogusch::Position position;
	UCI::parse_fen(fen, 0, position);
	// ------------temp
	// testing eval func

	//Pogusch::evaluate(position);
	//std::cout << position << std::endl;

	// ------------
	if(_print)
		std::cout << "depth: " << depth << std::endl;
	std::cout << debug::generate(position, depth, _print) << std::endl;
}

int debug::generate(Pogusch::Position position, int depth, bool print=0) {

	if(depth == 0)
		return 1;
	
	position.generate_positions();
	auto generated = position.get_next_positions();

	int sum = 0;
	bool _dbg = 0;
	for(auto [id, pos] : generated) {
		/*
		if(id == "e1c1") {
			_dbg = 1;
			std::cerr << (*pos) << std::endl;
		}
		else
			_dbg = 0;
		*/

		int now = debug::generate(*pos, depth-1, _dbg);
		if(print)
			std::cout << id << ": " << now << std::endl;
		sum += now;
	}
	
	return sum;
}