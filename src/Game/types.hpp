#pragma once

#include <iostream>
#include <utility>
#include <string>
#include <algorithm>
#include <vector>
#include <map>
#include <random>

using u64 = unsigned long long;

namespace Pogusch {
    const int SIZE_OF_BOARD = 8;

    enum class PieceType {
        None,
        Pawn,
        Knight,
        Bishop,
        Rook,
        Queen,
        King,
        Size = 7
    };

    std::string get_piece_symbol(PieceType& piece);
    std::ostream& operator<<(std::ostream& out, const PieceType& type);
    void operator++(PieceType& piece, int);

    enum class Color {
        None,
        White,
        Black,
        Size = 3 
    };

    std::ostream& operator<<(std::ostream& out, const Color& color);

    class Piece {
        private:
            PieceType type_of_piece = PieceType::None;
            Color color = Color::None;

        public:
            Piece();
            Piece(PieceType type_of_piece, Color color);

            char get_piece_symbol() const;
            PieceType get_type_of_piece() const;
            Color get_color() const;

            // it needs to be called (once) at the beginning of the execution
            static void initialize();
            static std::map<PieceType, std::vector<std::pair<int, int>>> velocity;
            static std::mt19937_64 rng;
            static std::vector<std::vector<std::vector<u64>>> hashes;
            static std::vector<u64> castling_hashes;
            static std::vector<u64> en_passant_hashes;
            static std::pair<PieceType, Color> char_to_piece(char c);
    };

    std::string get_uci_square(std::pair<int, int> square);
}
