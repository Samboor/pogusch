#include "uci.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstring>
#include <cctype>

#include "book.hpp"
#include "position.hpp"
#include "position_tree.hpp"
#include "types.hpp"

#ifndef DEBUG
#define DEBUG 0
#endif

void UCI::_debug_string(std::string s, int i) {
	std::cerr << "------------\n";
	std::cerr << s << '\n';
	std::cerr << s.size() << ' ' << i << '\n';
	if(i+1 >= (int)(s.size())) {
		std::cerr << "[debug]: unable to print the string\n";
		std::cerr << "------------\n";
		return;
	}
	std::cerr << "[" << s[i-1] << "], ";
	std::cerr << "[" << s[i  ] << "], ";
	std::cerr << "[" << s[i+1] << "]\n";
	std::cerr << "------------\n";
	std::cerr << std::flush;
}

// TODO: 
// fullmove counter
int UCI::parse_fen(std::string& cmd, int iter, Pogusch::Position& position) {
	int column = 0;
	int row = 7;

	while(iter<(int)(cmd.size()) && cmd[iter] != ' ') {
		if(cmd[iter] == '/') {
			column = 0;
			--row;
		}
		else if(isdigit(cmd[iter])) {
			column += cmd[iter]-'0';
		}
		else {
			if(DEBUG)
				std::cerr << "[" << column << ", " << row << "]: " << cmd[iter] << '\n';
			auto [type, color] = Pogusch::Piece::char_to_piece(cmd[iter]);
			position.place_piece({row, column}, type, color);
			++column;
		}
		++iter;
	}
	
	++iter;
	if(iter >= (int)(cmd.size()))
		return iter;
	if(cmd[iter] == 'w') {
		// white to move
		position.set_color(Pogusch::Color::White);
	}
	else {
		// black to move
		position.set_color(Pogusch::Color::Black);
	}

	iter+=2;
	bool black_long_castle, black_short_castle, white_long_castle, white_short_castle;
	black_long_castle = black_short_castle = white_long_castle = white_short_castle = 0;
	while(iter<(int)(cmd.size()) && cmd[iter] != ' ') {
		switch(cmd[iter]) {
			case 'K': {
				white_short_castle = 1;
				break;
			}
			case 'k': {
				black_short_castle = 1;
				break;
			}
			case 'Q': {
				white_long_castle = 1;
				break;
			}
			case 'q': {
				black_long_castle = 1;
				break;
			}
			default: {
				break;
			}
		}
		++iter;
	}

	position.set_castling_rights(white_short_castle, white_long_castle, black_short_castle, black_long_castle);

	++iter;
	if(iter >= (int)(cmd.size()))
		return iter;

	if(cmd[iter] != '-') {
		std::string en_passant = "xx";
		en_passant[0] = cmd[iter];
		++iter;
		en_passant[1] = cmd[iter];

		if(DEBUG) {
			std::cerr << "detected en passant: " << en_passant << ", column: " << en_passant[0]-'a' << '\n';
		}

		position.set_en_passant(en_passant[0]-'a');
	}

	iter+=2;
	std::string halfmove_string = "";
	std::string fullmove_string = "";
	while(iter<(int)(cmd.size()) && cmd[iter] != ' ') {
		halfmove_string.push_back(cmd[iter]);
		++iter;
	}
	
	int halfmove = 0;
	if(halfmove_string.size())
		halfmove = std::stoi(halfmove_string);

	position.set_number_of_halfmoves(halfmove);

	++iter;
	while(iter<(int)(cmd.size()) && cmd[iter] != ' ') {
		fullmove_string.push_back(cmd[iter]);
		++iter;
	}

	int fullmove = 0;
	if(fullmove_string.size())
		fullmove = std::stoi(fullmove_string);

	//position.set_number_of_fullmoves(fullmove);

	if(DEBUG) {
		std::cerr << halfmove << ' ' << fullmove << '\n';
	}

	return iter;
}

int UCI::parse_moves(std::string& cmd, int iter, Pogusch::Position& position, Pogusch::Book& book) {

	Pogusch::PositionTree tree(position);
	while(iter<(int)(cmd.size())) {
		std::string move = cmd.substr(iter, 4);
		iter+=4;
		if(iter < (int)(cmd.size()) && cmd[iter] != ' ') {
			move.push_back(cmd[iter]);
			++iter;
		}
		++iter;
		if(DEBUG)
			std::cerr << "[" << move << "]" << '\n';
		// todo: make move
		tree.make_move(move);
		book.advance(move);
	}

	position = *(tree.get_current_position());

	return iter;
}

void UCI::parse_position(std::string& cmd, Pogusch::Position& position, Pogusch::Book& book) {
	int iter = 9;
	if(cmd.substr(iter, 3) == "fen") {
		book.off_book();
		iter = parse_fen(cmd, 9+4, position);
	}
	else {
		std::string default_string = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
		parse_fen(default_string, 0, position);
		// startpos offset
		iter += 9;
	}

	if(DEBUG) {
		UCI::_debug_string(cmd, iter);
		std::cerr << position << std::endl;
	}

	if(iter >= (int)(cmd.size())) {
		position.generate_hash();
		return;
	}

	if(cmd.substr(iter, 5) == "moves") {
		iter = UCI::parse_moves(cmd, iter+6, position, book);
	}

	position.generate_hash();

	if(iter >= (int)(cmd.size()))
		return;
}

int UCI::get_number(int& iter, std::string& cmd) {
	int res = 0;
	while(iter < (int)cmd.size() && isdigit(cmd[iter])) {
		res *= 10;
		res += cmd[iter]-'0';
		++iter;
	}
	return res;
}

void UCI::parse_go(std::string& cmd, Pogusch::PositionTree* game, Pogusch::Book& book) {
	int iter = 3;

	const double thinking_time_fraction = 10;
	double thinking_time = 10;
	int wtime = 0;
	int btime = 0;
	int winc  = 0;
	int binc  = 0;

	while(iter < (int)(cmd.size())) {
		if(cmd.substr(iter, 8) == "movetime") {
			iter += 9;
			int time = get_number(iter, cmd);
			thinking_time = (double)time/1000. * thinking_time_fraction;
			++iter;
		}	
		else if(cmd.substr(iter, 5) == "wtime"){
			iter += 6;
			wtime = get_number(iter, cmd);
			++iter;
		}
		else if(cmd.substr(iter, 5) == "btime"){
			iter += 6;
			btime = get_number(iter, cmd);
			++iter;
		}
		else if(cmd.substr(iter, 4) == "winc"){
			iter += 5;
			winc = get_number(iter, cmd);
			++iter;
		}
		else if(cmd.substr(iter, 4) == "binc"){
			iter += 5;
			binc = get_number(iter, cmd);
			++iter;
		}
		else {
			break;
		}
	}

	if(std::min(wtime, btime) > 0) {
		if(game->get_current_position()->get_current_color() == Pogusch::Color::White)
			thinking_time = wtime;
		else
			thinking_time = btime;
	}

	if(book.in_book()) {
		std::string move = book.get_move();
		std::cout << "bestmove " << move << std::endl;
		std::cerr << "book: " << move << std::endl;
		game->make_move(move);
		return;
	}

	thinking_time = std::min(10.0, thinking_time/thinking_time_fraction);
	std::cerr << "thinking time: " << thinking_time << std::endl;
	game->search(thinking_time);
	std::cerr << "pos: " << game->get_depth_of_search() << std::endl;
	std::cerr << "evaluated: " << game->get_times_evaluated() << std::endl;
	std::cerr << "eval: " << game->get_current_position()->get_evaluation() << std::endl;
	std::cout << "bestmove " << game->make_move() << std::endl;
}

void UCI::loop() {
	std::cout << "id name Pogusch 0.1\n";
	std::cout << "id author Wiktor Cupial, Grzegorz Ryn\n";

	std::cout << "uciok\n";
	std::string cmd;
	Pogusch::Position position;
	Pogusch::PositionTree* game = NULL;
	// Pogusch::Book book("book.txt");
	Pogusch::Book book;
	while(1) {
		getline(std::cin, cmd); 
		if(cmd == "quit") {
			return;
		}
		else if(cmd == "isready") {
			std::cout << "readyok\n";
		}
		else if(cmd == "ucinewgame") {
			position = Pogusch::Position(0);
			delete game;
			game = NULL;
			book.init("book.txt");
			book.reset();
		}
		else if(cmd == "stop") {
			// ??
		}
		else if(cmd.substr(0, 2) == "go") {
			UCI::parse_go(cmd, game, book);
			// game = NULL;
		}
		else if(cmd.substr(0, 8) == "position") {

			if(game == NULL) {
				UCI::parse_position(cmd, position, book);
				game = new Pogusch::PositionTree(position);
			}
			else {
				int it = (int)(cmd.length()) - 4;
				if(isalpha(cmd.back())) {
					it--;
				}

				std::cerr << cmd.substr(it, 5) << std::endl;
				game->make_move(cmd.substr(it, 5));
				book.advance(cmd.substr(it, 5));
			}
		}
		else if(cmd == "print") {
			std::cout << position << std::endl;
			std::cout << *(game->get_current_position()) << std::endl;
		}
		else if(cmd != "" && cmd != " " && cmd != "\n") {
			std::cout << "unknown command: " << cmd << std::endl;
		}
	}
}
