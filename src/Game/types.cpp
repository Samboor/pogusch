#include "types.hpp"

#include <iostream>
#include <random>
#include <climits>
#include <utility>
#include <string>
#include <algorithm>
#include <vector>
#include <map>

namespace Pogusch {
    // PieceType

    std::string get_piece_symbol(PieceType& piece) {  
        std::string v="";
        switch(piece) {
            case PieceType::None: v = ""; break;
            case PieceType::Pawn: v = "p"; break;
            case PieceType::Knight: v = "n"; break;
            case PieceType::Bishop: v = "b"; break;
            case PieceType::Rook: v = "r"; break;
            case PieceType::Queen: v = "q"; break;
            case PieceType::King: v = "k"; break;
            default: v = '#'; break;
        }
        return v;
    }

    std::ostream& operator<<(std::ostream& out, const PieceType& type) {
        std::string v;

        switch(type) {
            case PieceType::None: v = "None"; break;
            case PieceType::Pawn: v = "Pawn"; break;
            case PieceType::Knight: v = "Knight"; break;
            case PieceType::Bishop: v = "Bishop"; break;
            case PieceType::Rook: v = "Rook"; break;
            case PieceType::Queen: v = "Queen"; break;
            case PieceType::King: v = "King"; break;
            default: v="[]"; break;
        }

        out << v;
        return out;
    }

    std::ostream& operator<<(std::ostream& out, const Color& color) {
        std::string v;
        switch(color) {
            case Color::None: v = "None"; break;
            case Color::White: v = "White"; break;
            case Color::Black: v = "Black"; break;
            default: v="[]"; break;
        }
        out << v;
        return out;
    }

    void operator++(PieceType& piece, int) {
        piece = static_cast<PieceType>(static_cast<int>(piece) + 1);
    }

    // Piece
    std::map<PieceType, std::vector<std::pair<int, int>>> Piece::velocity;

    std::mt19937_64 Piece::rng;
    std::vector<std::vector<std::vector<u64>>> Piece::hashes;
    std::vector<u64> Piece::castling_hashes;
    std::vector<u64> Piece::en_passant_hashes;
    void Piece::initialize() {
        velocity[PieceType::Bishop] = {{-1, -1}, {-1, 1}, {1, -1}, {1, 1}};

        velocity[PieceType::Knight] = {{-2, -1}, {-2, 1}, {-1, 2}, {1, 2}, {2, -1}, {2, 1}, {-1, -2}, {1, -2}};

        velocity[PieceType::Rook] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

        velocity[PieceType::King] = velocity[PieceType::Bishop];
        for(auto v : velocity[PieceType::Rook]) {
            velocity[PieceType::King].push_back(v);
        }

        velocity[PieceType::Queen] = velocity[PieceType::King];

        // zobrist init 
        rng=std::mt19937_64(time(NULL));
        hashes.resize(SIZE_OF_BOARD, std::vector<std::vector<u64>>(SIZE_OF_BOARD, std::vector<u64>(static_cast<int> ((PieceType::Size)))));
        for(int row=0; row<SIZE_OF_BOARD; ++row)
            for(int col=0; col<SIZE_OF_BOARD; ++col)
                for(PieceType piece=PieceType::Pawn; piece <= PieceType::King; piece++) {
                    hashes[row][col][static_cast<int>(piece)] = std::uniform_int_distribution<u64>(0, ULLONG_MAX)(rng);
                }
        
        castling_hashes.resize(1<<4);
        for(int i=1; i<(1<<4); ++i) 
            castling_hashes[i] = std::uniform_int_distribution<u64>(0, ULLONG_MAX)(rng);

        en_passant_hashes.resize(SIZE_OF_BOARD);
        for(int i=0; i<SIZE_OF_BOARD; ++i)
            en_passant_hashes[i] = std::uniform_int_distribution<u64>(0, ULLONG_MAX)(rng);

    }

    Piece::Piece() {}

    Piece::Piece(PieceType type_of_piece, Color color) : type_of_piece(type_of_piece), color(color) {}

    char Piece::get_piece_symbol() const {
        char symbol;

        switch(type_of_piece) {
            case PieceType::Pawn: {
                symbol = 'p';
                break;
            }
            case PieceType::Knight: {
                symbol = 'n';
                break;
            }
            case PieceType::Bishop: {
                symbol = 'b';
                break;
            }
            case PieceType::Rook: {
                symbol = 'r';
                break;
            }
            case PieceType::Queen: {
                symbol = 'q';
                break;
            }
            case PieceType::King: {
                symbol = 'k';
                break;
            }
            default: {
                symbol = ' ';
                break;
            }
        }

        if(color == Color::White) {
            return (char)toupper(symbol);
        }
        return symbol;
    }

    PieceType Piece::get_type_of_piece() const {
        return type_of_piece;
    }

    Color Piece::get_color() const {
        return color;
    }

	std::pair<PieceType, Color> Piece::char_to_piece(char c) {
		Color color = Color::Black;
		if(isupper(c)) {
			color = Color::White;
			c = (char)tolower(c);
		}
	
		PieceType type = PieceType::None;
		switch(c) {
			case 'p': {
				type = PieceType::Pawn;
				break;
			}
			case 'n': {
				type = PieceType::Knight;
				break;
			}
			case 'b': {
				type = PieceType::Bishop;
				break;
			}
			case 'r': {
				type = PieceType::Rook;
				break;
			}
			case 'q': {
				type = PieceType::Queen;
				break;
			}
			case 'k': {
				type = PieceType::King;
				break;
			}
			default: {
				break;
			}
		}

		return {type, color};
	}

    std::string get_uci_square(std::pair<int, int> square) {
        auto [row, col] = square;
        std::string coordinates;
        coordinates.push_back((char)(col + 'a'));
        coordinates.push_back((char)(row + '1'));
        return coordinates;
    }
}
