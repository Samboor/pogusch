#pragma once

#include <string>
#include <fstream>
#include <map>
#include <random>

namespace Pogusch {

struct Node {
    std::map<std::string, Node*> children;
    int pop = 0;
};

class Book {
private:
    std::fstream input;
    Node* root;
    Node* book;
    std::mt19937 rng;
    bool inited = false;
public:
    // Book() = delete;
    Book();
    Book(std::string file_name);
    ~Book();

    void init(std::string file_name);
    void off_book();
    bool in_book();
    bool advance(std::string move);
    std::string get_move();
    void reset();
    void __print_root();
    void __print_book();
};

}