#include "evaluation.hpp"
#include "position.hpp"
#include "types.hpp"
#include <vector>
#include <cmath>
#include <utility>

#ifndef DEBUG
#define DEBUG 0
#endif

// const double inf = HUGE_VAL;
const double inf = 1e4;

double Pogusch::color_val(const Color& c) {
    return (c==Color::Black?-1.:1.);
}

int Pogusch::color_idx(const Color& c) {
    return (c==Color::Black?1:0);
}

double Pogusch::value(const Pogusch::PieceType& type) {
    switch(type) {
        case PieceType::King:   return inf;
        case PieceType::Queen:  return 900.;
        case PieceType::Rook:   return 500.;
        case PieceType::Bishop: return 300.;
        case PieceType::Knight: return 300.;
        case PieceType::Pawn:   return 100.;
        default: return 0;
    }
}

std::pair<int, int> Pogusch::get_inv(int x, int y, Color c) {
    if(c == Color::Black)
        return {x, y};
    return {7-x, y};
}

double Pogusch::evaluate(const Pogusch::Position& position) {

    double eval = 0;
    std::vector<std::vector<int>> pawns_on_file(2, std::vector<int>(SIZE_OF_BOARD, 0));

    for(int row=0; row<SIZE_OF_BOARD; ++row)
        for(int col=0; col<SIZE_OF_BOARD; ++col) {

            auto p = position.get_piece({row, col});
            auto color = p.get_color();
            auto type  = p.get_type_of_piece();

            // count material
            eval += value(type)*color_val(color);
            
            // apply tables
            auto [x, y] = get_inv(row, col, color);
            switch(type) {
                case PieceType::King: {
                    eval += king_table_mg[x][y]*color_val(color);
                    break;
                }
                case PieceType::Queen: {
                    eval += queen_table[x][y]*color_val(color);
                    break;
                }
                case PieceType::Rook: {
                    eval += rook_table[x][y]*color_val(color);
                    break;
                }
                case PieceType::Bishop: {
                    eval += bishop_table[x][y]*color_val(color);
                    break;
                }
                case PieceType::Knight: {
                    eval += knight_table[x][y]*color_val(color);
                    break;
                }
                case PieceType::Pawn: {
                    ++pawns_on_file[color_idx(color)][col];
                    eval += pawn_table[x][y]*color_val(color);
                    break;
                }
                default: break;
            }
        }

    for(int i=0; i<2; ++i) {
        auto color = (i==0?Color::White:Color::Black);
        for(int col=0; col<SIZE_OF_BOARD; ++col) {
            if(pawns_on_file[i][col]==0)
                continue;
            if((col==0 || pawns_on_file[i][col-1]==0) && (col==7 || pawns_on_file[i][col+1]==0))
                eval += isolated_pawn*color_val(color);
            if(pawns_on_file[i][col] > 1)
                eval += stacked_pawns*color_val(color)*pawns_on_file[i][col];
        }
    }

    if(DEBUG)
        std::cout << "EVALUATION: " << eval << std::endl;
    return eval / 100;
}