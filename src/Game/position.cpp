#include "position.hpp"

#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>
#include <array>
#include <string>
#include <map>

#include "types.hpp"

namespace Pogusch {

    Position::Position(int move_count) {
        set_number_of_halfmoves(move_count);
    }

    Position::Position(const Position& position) {
        set_number_of_halfmoves(position.move_counter);

        for(int row = 0; row < SIZE_OF_BOARD; row++) {
            for(int column = 0; column < SIZE_OF_BOARD; column++) {
                board[row][column] = position.board[row][column];
            }
        }

        white_short_castle = position.white_short_castle;
        white_long_castle = position.white_long_castle;
        black_short_castle = position.black_short_castle;
        black_long_castle = position.black_long_castle;

        en_passant = position.en_passant;

        current_color = position.current_color;
        opposite_color = position.opposite_color;
    }

    Position& Position::operator=(const Position& position) {
        set_number_of_halfmoves(position.move_counter);

        for(int row = 0; row < SIZE_OF_BOARD; row++) {
            for(int column = 0; column < SIZE_OF_BOARD; column++) {
                board[row][column] = position.board[row][column];
            }
        }

        white_short_castle = position.white_short_castle;
        white_long_castle = position.white_long_castle;
        black_short_castle = position.black_short_castle;
        black_long_castle = position.black_long_castle;

        en_passant = position.en_passant;

        current_color = position.current_color;
        opposite_color = position.opposite_color;

        already_extended = false;
        next_positions.clear();

        return *this;
    }

    Position::~Position() {
        for(auto& next_position : next_positions) {
            delete next_position.second;
        }
    }

    const Piece& Position::get_piece(std::pair<int, int> square) const {
        auto [row, column] = square;
        return board[row][column];
    }

    void Position::place_piece(std::pair<int, int> square, PieceType type_of_piece, Color color) {
        auto [row, column] = square;
        board[row][column] = Piece(type_of_piece, color);
    }

    void Position::set_castling_rights(bool white_short_castle, bool white_long_castle, bool black_short_castle, bool black_long_castle) {
        this->white_short_castle = white_short_castle;
        this->white_long_castle = white_long_castle;
        this->black_short_castle = black_short_castle;
        this->black_long_castle = black_long_castle;
    }

    void Position::set_number_of_halfmoves(int number_of_halvmoves) {
        move_counter = number_of_halvmoves;
    }

    void Position::set_color(Color color) {
        if(color == Color::White) {
            current_color = Color::White;
            opposite_color = Color::Black;
        }
        else {
            current_color = Color::Black;
            opposite_color = Color::White;
        }
    }

    void Position::set_en_passant(int col) {
        en_passant = col;
    }

    void Position::generate_positions(bool only_captures) const {
        static std::map<Color, std::pair<int, int>> pawn_velocity = {{Color::White, {1, 0}}, {Color::Black, {-1, 0}}};
        static std::map<Color, int> starting_row = {{Color::White, 1}, {Color::Black, SIZE_OF_BOARD - 2}};
        static std::map<Color, int> promotion_row = {{Color::White, SIZE_OF_BOARD - 1}, {Color::Black, 0}};
        static std::map<Color, int> au_passant_row = {{Color::White, 4}, {Color::Black, 3}};

        if(already_extended == true) {
            return;
        }
        already_extended = true;

        std::array<std::array<bool, SIZE_OF_BOARD>, SIZE_OF_BOARD> captures = generate_captures(opposite_color);

        // "regular" moves
        for(int row = 0; row < SIZE_OF_BOARD; row++) {
            for(int column = 0; column < SIZE_OF_BOARD; column++) {
                PieceType type_of_piece = board[row][column].get_type_of_piece();
                Color color = board[row][column].get_color();
                if(color != current_color)
                    continue;
                switch(type_of_piece) {
                    case PieceType::None: {
                        break;
                    }
                    case PieceType::Pawn: {
                        auto [d_row, d_col] = pawn_velocity[current_color];

                        // Forward moves
                        if(row == starting_row[current_color] && empty(row + d_row, column) && empty(row + 2*d_row, column) && !only_captures) {
                            make_move({row, column}, {row + 2*d_row, column}, PieceType::Pawn, true);
                        }
                        else if(row + d_row == promotion_row[current_color] && empty(row + d_row, column) && !only_captures) { // promotion
                            for(PieceType new_piece = PieceType::Knight; new_piece <= PieceType::Queen; new_piece++) {
                                //std::cout << "\n\n" << type_of_piece << "->" << new_piece << std::endl;
                                make_move({row, column}, {row + d_row, column}, new_piece);
                            }
                        }

                        if(empty(row + d_row, column) && !(row+d_row==promotion_row[current_color]) && !only_captures) {
                            make_move({row, column}, {row + d_row, column});
                        }

                        // break; // ?

                        // Captures
                        for(int d_column = -1; d_column <= 1; d_column += 2) {
                            if(valid_square(row + d_row, column + d_column) && board[row + d_row][column + d_column].get_color() == opposite_color) {
                                if(row + d_row == promotion_row[current_color]) {
                                    for(PieceType new_piece = PieceType::Knight; new_piece <= PieceType::Queen; new_piece++) {
                                        make_move({row, column}, {row + d_row, column + d_column}, new_piece);
                                    }
                                }
                                else {
                                    //std::cerr << "pawn capture: " << row << ' ' << column << ' ' << row + d_row << ' ' << column+d_column << '\n';
                                    make_move({row, column}, {row + d_row, column + d_column});
                                }
                            }
                        }

                        // en passant :)
                        if(board[row][column].get_color() == current_color && au_passant_row[current_color] == row && en_passant != -1 && std::abs(en_passant - column) == 1) {
                            //std::cerr << "en passant: " << std::endl;
                            Position *new_position = new Position(*this);
                            if(current_color == Color::Black)
                                new_position->set_number_of_halfmoves(move_counter + 1);

                            new_position->place_piece({row, column}, PieceType::None, Color::None);
                            new_position->place_piece({row + d_row, en_passant}, PieceType::Pawn, current_color);
                            new_position->place_piece({row, en_passant}, PieceType::None, Color::None);
                            new_position->en_passant=-1;
                            std::swap(new_position->current_color, new_position->opposite_color);

                            new_position->parent = this;

                            if(new_position->valid()) {
                                next_positions.push_back({get_uci_square({row, column}) + get_uci_square({row + d_row, en_passant}), new_position});
                            }
                            else {
                                delete new_position;
                            }
                        }
                        break;
                    }
                    default: {
                        for(std::pair<int, int> vel : Piece::velocity[type_of_piece]) {
                            auto [d_row, d_col] = vel;
                            int move_iterations = SIZE_OF_BOARD;
                            if(type_of_piece == PieceType::King || type_of_piece == PieceType::Knight) {
                                move_iterations = 1;
                            }
                            
                            for(int t = 1; t <= move_iterations; t++) {
                                if(valid_square(row + t*d_row, column + t*d_col) && board[row + t*d_row][column + t*d_col].get_color() != current_color) {
                                    //std::cerr << row << ' ' << column << ' ' << type_of_piece << std::endl;
                                    if(board[row + t*d_row][column + t*d_col].get_color() == Color::None && only_captures)
                                        continue;
                                    make_move({row, column}, {row + t*d_row, column + t*d_col});
                                }
                                if(valid_square(row + t*d_row, column + t*d_col) && board[row + t*d_row][column + t*d_col].get_color() != Color::None) {
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        if(only_captures)
            return;

        // Castling
        castle({0, 4}, {0, SIZE_OF_BOARD - 1}, white_short_castle, captures);
        castle({0, 4}, {0, 0}, white_long_castle, captures);
        castle({SIZE_OF_BOARD - 1, 4}, {SIZE_OF_BOARD - 1, SIZE_OF_BOARD - 1}, black_short_castle, captures);
        castle({SIZE_OF_BOARD - 1, 4}, {SIZE_OF_BOARD - 1, 0}, black_long_castle, captures);
    }

    bool Position::valid() const {
        std::pair<int, int> king;
        std::array<std::array<bool, SIZE_OF_BOARD>, SIZE_OF_BOARD> captures = generate_captures(current_color);

        for(int row = 0; row < SIZE_OF_BOARD; row++) {
            for(int col = 0; col < SIZE_OF_BOARD; col++) {
                if(board[row][col].get_color() != opposite_color) {
                    continue;
                }
                if(board[row][col].get_type_of_piece() == PieceType::King && captures[row][col]) {
                    return false;
                }
            }
        }

        return true;
    }

    void Position::generate_hash() {
        hash = 0;
        for(int row=0; row<SIZE_OF_BOARD; ++row)
            for(int col=0; col<SIZE_OF_BOARD; ++col) {
                auto piece = board[row][col].get_type_of_piece();
                if(piece == PieceType::None)
                    continue;
                hash ^= Piece::hashes[row][col][static_cast<int>(piece)];
            }

        int mask=0;
        if(white_short_castle)
            mask ^= 1;
        if(black_short_castle)
            mask ^= (1<<1);
        if(white_long_castle)
            mask ^= (1<<2);
        if(black_long_castle)
            mask ^= (1<<3);
        hash ^= Piece::castling_hashes[mask];

        if(en_passant != -1)
            hash ^= Piece::en_passant_hashes[en_passant];
    }

    u64 Position::get_hash() const {
        return hash;
    }

    void Position::reset() {
        already_extended=0;
        get_next_positions().clear();
        get_next_positions().shrink_to_fit();
    }

    std::array<std::array<bool, SIZE_OF_BOARD>, SIZE_OF_BOARD> Position::generate_captures(Color color) const {
        std::array<std::array<bool, SIZE_OF_BOARD>, SIZE_OF_BOARD> captures;
        for(int row = 0; row < SIZE_OF_BOARD; row++) {
            std::fill(captures[row].begin(), captures[row].end(), false);
        }

        for(int row = 0; row < SIZE_OF_BOARD; row++) {
            for(int col = 0; col < SIZE_OF_BOARD; col++) {
                if(board[row][col].get_color() != color) {
                    continue;
                }
                
                //std::cerr << row << ' ' << col << std::endl;
                if(board[row][col].get_type_of_piece() == PieceType::Pawn) {
                    int d_row = 1;
                    if(color == Color::Black) {
                        d_row = -1;
                    }

                    for(int d_col = -1; d_col <= 1; d_col += 2) {
                        if(valid_square(row + d_row, col + d_col) && board[row + d_row][col + d_col].get_color() != color) {
                            captures[row + d_row][col + d_col] = true;
                        }
                    }
                    continue;
                }
                
                for(auto [d_row, d_col] : Piece::velocity[board[row][col].get_type_of_piece()]) {
                    int move_iterations = SIZE_OF_BOARD;
                    if(board[row][col].get_type_of_piece() == PieceType::King || board[row][col].get_type_of_piece() == PieceType::Knight) {
                        move_iterations = 1;
                    }
                    for(int t = 1; t <= move_iterations; t++) {
                        if(valid_square(row + t*d_row, col + t*d_col) && board[row + t*d_row][col + t*d_col].get_color() != color) {
                            captures[row + t*d_row][col + t*d_col] = true;
                        }
                        if(valid_square(row + t*d_row, col + t*d_col) && board[row + t*d_row][col + t*d_col].get_color() != Color::None) {
                            break;
                        }
                    }
                }
            }
        }

        return captures;
    }

    // Utility for move generator
    bool Position::empty(int row, int col) const {
        return board[row][col].get_type_of_piece() == PieceType::None;
    }

    bool Position::valid_square(int row, int col) const {
        return 0 <= std::min(row, col) && std::max(row, col) < SIZE_OF_BOARD;
    }

    void Position::castle(std::pair<int, int> king, std::pair<int, int> rook, bool castling_right, std::array<std::array<bool, SIZE_OF_BOARD>, SIZE_OF_BOARD>& captures) const {
        auto sgn = [&](int x) {
            if(x < 0) {
                return -1;
            }
            else if(x == 0) {
                return 0;
            }
            return 1;
        };

        if(castling_right == false) {
            return;
        }

        auto [row_king, col_king] = king;
        auto [row_rook, col_rook] = rook;
        if(board[row_rook][col_rook].get_type_of_piece() != PieceType::Rook)
            return;
        int dir =  sgn(col_rook - col_king);

        if(board[row_king][col_king].get_color() != current_color) {
            return;
        }
        
        for(int col = col_king+dir; col != col_rook; col += dir) {
            if(board[row_king][col].get_type_of_piece() != PieceType::None) { // || captures[row_king][col]) {
                return;
            }
        }

        if(captures[row_king][col_king] || captures[row_king][col_king + dir] || captures[row_king][col_king + 2*dir]) {
            return;
        }

        Position *new_position = new Position(*this);
        if(current_color == Color::Black)
            new_position->set_number_of_halfmoves(move_counter + 1);
        new_position->place_piece({row_king, col_king}, PieceType::None, Color::None);
        new_position->place_piece({row_king, col_king + 2*dir}, PieceType::King, current_color);
        new_position->place_piece({row_rook, col_rook}, PieceType::None, Color::None);
        new_position->place_piece({row_rook, col_king + dir}, PieceType::Rook, current_color);
        new_position->set_color(opposite_color);
        new_position->en_passant=-1;
            
        if(current_color == Color::White) {
            if(dir == 1) {
                new_position->white_short_castle = false;
            }
            else {
                new_position->white_long_castle = false;
            }
        }
        else {
            if(dir == 1) { 
                new_position->black_short_castle = false;
            }
            else {
                new_position->black_long_castle = false;
            }
        }

        new_position->parent = this;

        // Prob. can be skipped
        if(new_position->valid()) {
            next_positions.push_back({get_uci_square(king) + get_uci_square({row_king, col_king + 2*dir}), new_position});
        }
        else {
            delete new_position;
        }
    }

    void Position::make_move(std::pair<int, int> begin, std::pair<int, int> end, PieceType new_piece, bool au_passant_flag) const {
        if(get_piece(begin).get_color() == opposite_color) {
            return;
        }
        
        std::string prom = "";
        Position *new_position = new Position(*this);
        if(new_piece == PieceType::None) {
            new_piece = get_piece(begin).get_type_of_piece();
        }
        else if(new_piece != PieceType::Pawn) {
            prom = get_piece_symbol(new_piece);
        }

        if(au_passant_flag == true) {
            new_position->en_passant = begin.second;
        }
        else
            new_position->en_passant = -1;

        new_position->place_piece(begin, PieceType::None, Color::None);
        new_position->place_piece(end, new_piece, current_color);
        if(current_color == Color::Black)
            new_position->set_number_of_halfmoves(move_counter + 1);
        new_position->set_color(opposite_color);

        // Castling update
        if(new_piece == PieceType::King) {
            if(current_color == Color::White) {
                new_position->white_short_castle = false;
                new_position->white_long_castle = false;
            }
            else {
                new_position->black_short_castle = false;
                new_position->black_long_castle = false;
            }
        }
        else if(new_piece == PieceType::Rook) {
            if(begin == std::make_pair(0, 0)) {
                new_position->white_long_castle = false;
            }
            else if(begin == std::make_pair(0, SIZE_OF_BOARD - 1)) {
                new_position->white_short_castle = false;
            }
            else if(begin == std::make_pair(SIZE_OF_BOARD - 1, 0)) {
                new_position->black_long_castle = false;
            }
            else if(begin == std::make_pair(SIZE_OF_BOARD - 1, SIZE_OF_BOARD - 1)) {
                new_position->black_short_castle = false;
            }
        }

        new_position->parent = this;

        // Validation 
        if(new_position->valid()) {
            next_positions.push_back({get_uci_square(begin) + get_uci_square(end) + prom, new_position});  
        }
        else {  
            //std::cerr << "Discarded!:\n";
            //std::cerr << *new_position << std::endl;
            delete new_position;
        }
    }

    bool Position::check() const {
        auto captures = generate_captures(opposite_color);

        bool king_under_check = false;
        for(int row = 0; row < SIZE_OF_BOARD; row++) {
            for(int col = 0; col < SIZE_OF_BOARD; col++) {
                if(board[row][col].get_color() != current_color) {
                    continue;
                }
                if(board[row][col].get_type_of_piece() != PieceType::King) {
                    continue;
                }

                if(captures[row][col] == true) {
                    king_under_check = true;
                }
            }
        }

        return king_under_check;
    }

    bool Position::stalemate() const {
        generate_positions();
        return next_positions.size() == 0 && !check();
    }

    bool Position::checkmate() const {
        generate_positions();
        return next_positions.size() == 0 && check();
    }

    std::vector<std::pair<std::string, Position*>>& Position::get_next_positions() const {
        return next_positions;
    }

    const Color& Position::get_current_color() const {
        return current_color;
    }

    int Position::get_move_counter() const {
        return move_counter;
    }

    double Position::get_evaluation() const {
        return evaluation;
    }

    void Position::set_evaluation(double eval) const {
        evaluation = eval;
    }

    const Position* Position::get_parent() const {
        return parent;
    }

    std::ostream& operator<<(std::ostream& out, const Position& position) {
        //const static std::string empty_row(2 * SIZE_OF_BOARD + 1, '-');
        std::string empty_row = "+---+";
        for(int i=0; i<7; ++i)
            empty_row += "---+";

        out << "Position " << std::endl;
        
        out << "  " << empty_row << std::endl;
        for(int row = SIZE_OF_BOARD - 1; row >= 0; row--) {
            out << row+1 << " | ";
            for(int column = 0; column < SIZE_OF_BOARD; column++) {
                out << position.get_piece({row, column}).get_piece_symbol() << " | ";
            }
            out << std::endl;
            out << "  " << empty_row << std::endl;
        }

        out << "    ";
        for(char column = 'a'; column < 'a' + SIZE_OF_BOARD; column++) {
            out << column << "   ";
        }
        out << std::endl;
        out << std::endl;
        out << "on move: " << position.current_color << std::endl;
        out << "en passant: " << position.en_passant << std::endl;
        out << "hash: " << position.hash << std::endl;

        return out;
    }
}

