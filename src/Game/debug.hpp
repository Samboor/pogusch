#pragma once

#include "position.hpp"

namespace debug {

void perft();
int generate(Pogusch::Position position, int depth, bool print);

};
