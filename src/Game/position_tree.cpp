#include "position_tree.hpp"

#include <cmath>
#include <iostream>
#include <string>
#include <ctime>
#include <iomanip>
#include <unordered_map>
#include <utility>
#include <functional>
#include <algorithm>
#include <malloc.h>
#include <random>

#include "types.hpp"
#include "position.hpp"
#include "evaluation.hpp"

namespace Pogusch {

    PositionTree::PositionTree(const Position& position) {
        dp.clear();
        root = new Position(position);
        current_position = root;

        current_position->set_evaluation(evaluate(*current_position));
    }

    PositionTree::~PositionTree() {
        delete root;
    }

    bool PositionTree::compare_pointers_to_position::operator() (const std::pair<std::string, Position*>& lhs, const std::pair<std::string, Position*>& rhs) const {
        auto sgn = [](Position* pos) {
            double sign = 1;
            if(pos->get_current_color() == Color::Black) {
                sign = -1;
            }
            return sign;
        };

        double lhs_eval = lhs.second->get_evaluation() * sgn(lhs.second);
        double rhs_eval = rhs.second->get_evaluation() * sgn(rhs.second);

        return lhs_eval < rhs_eval;
    }

    void PositionTree::cleanup(std::unordered_map<u64, double>& x) {
        std::unordered_map<u64, double> tmp;
        swap(x, tmp);
        return;
    }

    void PositionTree::search(double time) {
        const int MAX_DEPTH = 5;
        std::clock_t starting_time = std::clock();
        times_evaluated = 0;

        while((double)(std::clock() - starting_time)/CLOCKS_PER_SEC < time && current_depth_of_search < MAX_DEPTH) {
            std::cerr << "EXTEND :) " << std::endl;
            cleanup(dp);
            extend_tree();
            malloc_trim(0);
            current_depth_of_search++;
        }
    }

    std::string PositionTree::make_move(std::string move) {
        current_position->generate_positions();
        if(move.length() == 0) {
            auto& pos_list = current_position->get_next_positions();
            move = pos_list[0].first;
        }

        Position* new_position = NULL;
        
        for(auto [uci_move, position] : current_position->get_next_positions()) {
            if(uci_move != move) {
                delete position;
            }
            else {
                new_position = position;
            }
        }

        current_position->get_next_positions() = {{move, new_position}};
        current_position = new_position;
        current_depth_of_search--;

        return move;
    }

    int PositionTree::extend_tree() {
        const double inf = HUGE_VAL;

        std::function<int(Position*, double, double)> alpha_beta_only_captures = [&] (Position* position, double a, double b) {
            Color color_of_opponent = Color::Black;
            if(position->get_current_color() == Color::Black) {
                color_of_opponent = Color::White;
            }

            auto sign = [](Color color) {
                int sgn = 1;
                if(color == Color::Black) {
                    sgn = -1;
                }
                return sgn;
            };

            if(position->checkmate()) {
                position->set_evaluation(inf * sign(color_of_opponent));
                return 1;
            }
            if(position->stalemate()) {
                position->set_evaluation(0);
                return 1;
            }

            //tmp 
            position->generate_hash();
            if(dp.count(position->get_hash())) {
                position->set_evaluation(dp[position->get_hash()]);
                return 1;
            }
            
            int subtree_size = 0;
            position->reset();
            position->generate_positions(1);
            auto& pos_list = position->get_next_positions();
            //std::cerr << *position << '\n';
            //std::cerr << pos_list.size() << '\n';
            if(pos_list.size() == 0) {
                //std::cerr << "reached end in only_c!\n";
                //std::cerr << *position << '\n';
                position->set_evaluation(evaluate(*position));
                dp[position->get_hash()] = position->get_evaluation();
                ++times_evaluated;
                std::cerr << times_evaluated << std::endl;
                return 1;
            }
            if(position->get_current_color() == Color::White) {
                double eval = -inf;
                auto max_it = pos_list.begin();
                for(int i = 0; i < (int)pos_list.size(); i++) {
                    auto [uci_move, pos] = pos_list[i];
                    subtree_size += alpha_beta_only_captures(pos, a, b);

                    if(pos->get_evaluation() > eval) {
                        eval = pos->get_evaluation();
                        max_it = pos_list.begin() + i;
                    }
                    if(eval >= b) {
                        break;
                    }
                    a = std::max(a, eval);
                }
                position->set_evaluation(eval);
                dp[position->get_hash()] = position->get_evaluation();
                std::swap(*pos_list.begin(), *max_it);
            }
            else {
                double eval = inf;
                auto min_it = pos_list.begin();
                for(int i = 0; i < (int)pos_list.size(); i++) {
                    auto [uci_move, pos] = pos_list[i];
                    subtree_size += alpha_beta_only_captures(pos, a, b);

                    if(pos->get_evaluation() < eval) {
                        eval = pos->get_evaluation();
                        min_it = pos_list.begin() + i;
                    }
                    if(eval <= a) {
                        break;
                    }
                    b = std::min(b, eval);
                }
                position->set_evaluation(eval);
                dp[position->get_hash()] = position->get_evaluation();
                std::swap(*pos_list.begin(), *min_it);
            }
            position->reset();
            return subtree_size;
        };

        std::function<int(Position*, int, double, double)> alpha_beta = [&] (Position* position, int depth, double a, double b) {
            Color color_of_opponent = Color::Black;
            if(position->get_current_color() == Color::Black) {
                color_of_opponent = Color::White;
            }

            auto sign = [](Color color) {
                int sgn = 1;
                if(color == Color::Black) {
                    sgn = -1;
                }
                return sgn;
            };

            if(position->checkmate()) {
                position->set_evaluation(inf * sign(color_of_opponent));
                return 1;
            }
            if(position->stalemate()) {
                position->set_evaluation(0);
                return 1;
            }

            //tmp 
            position->generate_hash();
            if(dp.count(position->get_hash())) {
                position->set_evaluation(dp[position->get_hash()]);
                return 1;
            }

            if(depth == 0) {
                Position* p2 = new Position(*position);
                alpha_beta_only_captures(p2, a, b);
                position->set_evaluation(p2->get_evaluation());
                dp[position->get_hash()] = position->get_evaluation();
                p2->reset();
                delete p2;
                malloc_trim(0);
                //position->set_evaluation(evaluate(*position));
                return 1;
            }

            int subtree_size = 0;
            position->generate_positions();
            auto& pos_list = position->get_next_positions();
            std::shuffle(pos_list.begin(), pos_list.end(), Piece::rng);
            if(position->get_current_color() == Color::White) {
                double eval = -inf;
                auto max_it = pos_list.begin();
                for(int i = 0; i < (int)pos_list.size(); i++) {
                    auto [uci_move, pos] = pos_list[i];
                    subtree_size += alpha_beta(pos, depth-1, a, b);

                    if(pos->get_evaluation() > eval) {
                        eval = pos->get_evaluation();
                        max_it = pos_list.begin() + i;
                    }
                    if(eval >= b) {
                        break;
                    }
                    a = std::max(a, eval);
                }
                position->set_evaluation(eval);
                dp[position->get_hash()] = position->get_evaluation();
                std::swap(*pos_list.begin(), *max_it);
            }
            else {
                double eval = inf;
                auto min_it = pos_list.begin();
                for(int i = 0; i < (int)pos_list.size(); i++) {
                    auto [uci_move, pos] = pos_list[i];
                    subtree_size += alpha_beta(pos, depth-1, a, b);

                    if(pos->get_evaluation() < eval) {
                        eval = pos->get_evaluation();
                        min_it = pos_list.begin() + i;
                    }
                    if(eval <= a) {
                        break;
                    }
                    b = std::min(b, eval);
                }
                position->set_evaluation(eval);
                dp[position->get_hash()] = position->get_evaluation();
                std::swap(*pos_list.begin(), *min_it);
            }

            return subtree_size;
        };

        return alpha_beta(current_position, current_depth_of_search + 1, -inf, inf);
    }

    const Position* PositionTree::get_current_position() const {
        return current_position;
    }

    int PositionTree::get_depth_of_search() const {
        return current_depth_of_search;
    }

    int PositionTree::get_times_evaluated() const {
        return times_evaluated;
    }
}