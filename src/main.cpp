#include <iostream>

#include "Game/uci.hpp"
#include "Game/debug.hpp"

int32_t main() {

	Pogusch::Piece::initialize();
	std::string cmd;
	while(1) {
		std::cin >> cmd;
		if(cmd == "exit" || cmd == "quit" || cmd == "q") {
			return 0;
		}
		else if(cmd == "uci") {
			UCI::loop();
		}
		else if(cmd == "perft") {
			debug::perft();
			return 0;
		}
		else {
			std::cout << "Unknown command: " << cmd << std::endl;
		}
	}

	return 0;
}
