cc = g++
exe = pogusch.exe
src = $(wildcard src/*.cpp src/*/*.cpp)
obj = $(subst .cpp,.o,$(src))
CXXFlags = -Wall -Wextra -Wunused -Wconversion -std=c++17 -O3

$(exe): $(obj)
	$(cc) $(obj) -fsanitize=address -o $(exe)

%.o : %.cpp
	$(cc) $(CXXFlags) -c $< -o $@

clean:
	-rm *.exe
	-rm src/*.o
	-rm src/*/*.o

debug: CXXFlags += -g -DDEBUG -fsanitize=address
debug: $(exe)
