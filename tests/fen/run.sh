#!/bin/bash

for ts in *.in
do
	name=`echo "$ts" | cut -d'.' -f1`
	out=${name}.out
	echo "Running test: ${name}"
	../../pogusch.exe < $ts > tmp.out 2> /dev/null
	tail -n +4 tmp.out > prog.out
	cmp $out prog.out > /dev/null
	if (($? != 0))
	then
		echo "$(tput setaf 9)Failed"
		echo "$(tput setaf 15)Expected: "
		cat $out
		echo "Output: "
		cat prog.out
		break
	else
		echo "$(tput setaf 10)Passed"
	fi
	echo "$(tput setaf 15)"
done

rm tmp.out
rm prog.out
